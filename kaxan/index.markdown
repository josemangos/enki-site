---
layout: kaxan
title: "KAXAN DNSSEC"
slug: KAXAN-DNSSEC
date:   2014-06-04 18:56:17
header: projects
---

Introducción
-------------

La herramienta *Kaxan dnssec* permite verificar la configuración de un archivo de zona DNS. Kaxan dnssec pretende ayudar a la búsqueda y solución de errores de configuración o inconsistencias al momento de firmar la zona. Realiza un conjunto de pruebas verificando las firmas RRSIG, los registros NSEC o NSEC3, la existencia de Glue Records y la cadena NSEC o NSEC3.  El validador Kaxan dnssec surge de la necesidad de utilizar una herramienta hecha para validar un archivo de zona que utiliza DNSSEC.


Está herramienta está diseñada para ayudar a administradores de una zona DNS a diagnosticar sus archivos de zona y verificar el correcto uso de la tecnología DNSSEC y verificar que estén correctamente firmadas. El proceso de validación consiste en un conjunto de pruebas, las cuales son:


* Verificación de sintaxis de cada registro DNS
* Existencia de todas las firmas.
* Verificar que las firmas RRSIG sean válidas.
* Existencia de todos los registros NSEC o NSEC3 y que además estén correctamente constituidos.
* Verifica que la que la cadena NSEC/NSEC3 esté elaborada correctamente.
* Verifica que los registros NS/MX pueden ser resueltos.


Antecendentes
-------------

### DNS



El sistema de nombres de dominio (DNS) puede compararse como un directorio telefónico en el cual se mantiene un seguimiento de asignaciones de nombres (entendibles y fáciles de recordar para los humanos) y direcciones IP. En este directorio un nombre de una página web como **www.kaxan-dnssec.mx** es asignado a una dirección IP, la cual necesitará el navegador para iniciar la comunicación con el servidor de la página. 

Además es importante saber, que el DNS no sólo sirve como una base de datos para saber las direcciones IP de los nombres de sitios web, sino que es un sistema fundamental para el funcionamiento y operación de la Internet, un ejemplo es que el DNS también hace posible la transmisión de correos electrónicos, mensajería instantánea y voz sobre IP.

Los nombres de dominio están asociados a las direcciones IP, dichas asociaciones nos almacenadas en servidores especiales, conocidos como servidores de nombre de dominio.

El DNS es un sistema distribuido y jerárquico, diferentes empresas administran una parte de la base de datos y tiene una estructura similar a la de un árbol invertido, cuyo primer nivel es conocido como la zona raíz (root zone) donde se encuentra la información de los servidores de dominio de nivel superior (top-level domain) tales como: .com, .mx, .org, etc. 


Para resolver el dominio  **www.kaxan-dnssec.mx**, el servidor recursivo le pide a la zona raíz  dónde encontrar información sobre .mx. Después de que obtiene una respuesta, le pide al servidor .mx dónde encontrar información sobre zoneFileInspector.mx y, finalmente, pide la dirección de www.zoneFileInspector.mx al servidor de la zona zoneFileInspector.mx. Después de ese proceso se proporciona la dirección completa al usuario.


### DNSSEC

Desde la implementación del DNS a la fecha se han descubiertos varias vulnerabilidades y ataques, una causa de esto fue que cuando se desarrolló no se incluyó ningún tipo de seguridad. Como resultado, un sistema de seguridad fue ideado en forma de extensiones que se puede añadir al protocolo existente de DNS. Este sistema fue posteriormente aprobado como un estándar por el Internet Engineering Task Force (IETF).


DNSSEC es una tecnología que se ha desarrollado para brindar protección contra ataques mediante la firma digital de los datos a fin de tener la seguridad de que son válidos. Es importante destacar, que DNSSEC no cifra los mensajes DNS, simplemente confirma la validez de los datos. DNSSEC no ofrece protección sobre cómo se envían los datos o quién tiene acceso a ellos, sino que autentica el origen de los datos enviados desde un servidor DNS, comprueba su integridad y autentica los datos que no existen.

### ¿Por qué usar DNSSEC?

DNSSEC nos proporciona una manera de estar seguros de que nos estamos comunicando con el sitio correcto. Antes de conectarse a un sitio web, el navegador tiene que recuperar la dirección IP del sitio, utilizando DNS. Sin embargo, es posible para un atacante interceptar las consultas DNS y proporcionar información falsa que causaría que el navegador se conecté a un sitio web falso donde se puede potencialmente proporcionar información personal. DNSSEC provee un nivel de seguridad para asegurarse de que la información de DNS es correcta y no se modificó. El servidor de DNS local realizará la validación de DNSSEC, con ayuda del servidor recursivo. En caso de que una respuesta DNS no pase la validación ésta se descartará.


Usar DNSSEC asegurará al usuario final que se conecta con el sitio web o servicio correspondiente al nombre de dominio al que trata de conectarse. Sin embargo, esto no va resolver todos los problemas de seguridad de internet, sí protege una parte crítica (las consultas al DNS) pero se deberá complementar con otras tecnologías como SSL que protegen la comunicación de datos.

### Configuración de DNSSEC

Para hacer uso de la tecnología DNSSEC debe tomar en cuanta al menos los siguientes pasos:

* Deberá firmar su archivo de zona.
* Verificar que la versión e implementación del servidor DNS soporte DNSSEC.
* Habilitar el uso de DNSSEC en los servidores DNS.
* Verificar que la red soporte mensajes DNS más grandes.

### Registros DNSSEC

DNSSEC usa llaves públicas y firmas digitales para verificar los datos y llaves privadas para generar las firmas. DNSSEC no puede proteger la privacidad o confidencialidad de los datos, ya que el costo para responder un paquete se incrementaría considerablemente. Su objetivo es enviar las llaves y firmas necesarias para autenticar los datos DNS o la no existencia de estos. Esta implementación requiere de nuevos tipos de registros DNS, los cuales son:

* DS (Deletation Signer): Es un hash que identifica la llave pública de la zona a delegar.
* DNSKEY: Llave pública utilizada para validar los registros RRSIG.
* NSEC: se utiliza para frustrar los intentos de suplantación de DNS certificando la no existencia de datos. 
* RRSIG: Es la firma digital correspondiente a un Resourse Record Set y contiene información necesaria para realizar la validación: llave que genero la firma y el algoritmo. 


El propósito original de DNSSEC es proteger a los clientes de Internet de la falsificación de datos DNS mediante la verificación de las firmas digitales incluidas en los datos. Si las firmas digitales son válidas, entonces se le permite pasar al usuario los datos que solicitó. 


### Cadena de confianza

La verificación de respuestas DNSSEC requiere de un punto de partida llamado Trust Anchor. Desde el 2010, cuando la root zone se firmó y se generaron sus llaves KSK y ZSK, podemos contar con el Root Trust Anchor, el cual puede encontrarse en [https://www.iana.org/dnssec](https://www.iana.org/dnssec). Después de que la llave que firmó los datos se obtiene, y estos a su vez, fueron verificados, dicha llave debe ser verificada por el servidor a través de la cadena de confianza, que consiste en una serie de registros DS y registros DNSKEY comenzando con el Trust Anchor.

DNSSEC no cifra los datos y es compatible con el DNS y las aplicaciones actuales. No cambia los protocolos existentes sobre los que se basa el DNS. Incorpora una cadena de firmas digitales en la que cada nivel del árbol DNS posee sus propias llaves de generación de firmas. En cada nivel del árbol DNS cada entidad debe firmar la llave del nivel de abajo. Por ejemplo: .mx firma la llave de la zona zoneFileInspector.mx y zona raíz firma la llave de .mx. 

Durante la validación de una respuesta DNSSEC, se sigue esta cadena de confianza validando las llaves del nivel inferior con las llaves del nivel padre, comenzando por la raíz. Debido a que cada llave puede ser validada por la de arriba, la única llave necesaria para validar los datos DNS sería la llave de la raíz. 

### NSEC3

En DNSSEC, a todos los nombres en una zona se les genera un registro NSEC, los cuales se ordenan formando una cadena de los registros firmados. Estos registros son firmados con la llave privada de la zona. Los registros NSEC son usados para acreditar la no existencia de un registro o para autenticar respuestas negativas.

Mientras que DNSSEC incrementa la seguridad en internet, se ha descubierto que causa una nueva vulnerabilidad, que se conoce como enumeración de zona. Si un atacante puede obtener la información completa de una zona, como la información de sitios web y servidores especiales, éste tendría la posibilidad de preparar y ejecutar un ataque de Internet en menor tiempo. NSEC3 (RFC 5155) se desarrolló para abordar esta cuestión, NSEC3 mitiga, pero no elimina, la enumeración de la zona, ya que es posible buscar de manera exhaustiva el conjunto de todos los nombres posibles en una zona.

Los registros NSEC3 incluyen un hash del nombre de dominio en lugar de poseer el nombre directamente, esto para evitar el ataque de enumeración de zona.  Además, el registro NSEC3 posee un número de iteraciones y una SALT, los cuales complican los ataques de diccionario pre-calculado.

### KSK y ZSK

Administrativamente hablando, los registros DNSKEY se dividen en dos tipos: Key Signing Key (KSK) y Zone Signing Key (ZSK). La llave de tipo KSK es una llave de largo plazo, mientras que ZSK es una llave corto plazo. 

En el caso de la criptografía asimétrica utilizada en DNSSEC, un atacante puede comprometer, a través de fuerza bruta u otros métodos, la llave privada utilizada para crear las firmas que acreditan la validez de los registros DNS. Para mitigar este riesgo, DNSSEC hace uso de dos tipos de llaves KSK y ZSK. La llave ZSK es utilizada para calcular rutinariamente firmas para los registros DNS y la llave KSK para calcular las firmas de la llave ZSK. 

La llave ZSK se cambia con frecuencia para frustrar los intentos de ataque y hacer que sea difícil de adivinar, mientras que la llave KSK se cambia en un período de tiempo mayor, generalmente en el orden de años. Dado que la KSK firma la ZSK y la ZSK firma los registros DNS, sólo la KSK requiere ser enviada a la zona padre, en forma de un registro DS. La zona padre firma el registro DS con su propia ZSK que es firmado por su propia KSK, formando así la cadena de confianza.

Instalación
-----------

* Descargar:
* Configurar:
    * Logging: logging.properties
* Compilar y generar binarios.
    * ant clean
    * ant export
* Ejecutar:

```
java -jar mx-dnssec-validator.jar
```

Operación
---------
DNSSEC Zone Inspector verifica el estado y la configuración DNSSEC de un archivo de zona. Provee un reporte con los posibles errores de configuración y las sugerencias sobre cómo resolverlos. Implementar DNSSEC de manera correcta no es sencillo y es un proceso propenso a errores, se deberá mantener las llaves, realizar los cambios de llaves, verificar que la zona padre tenga el registro DS correcto, firmar cada registro nuevo, etc. DNSSEC Zone Inspector será de gran ayuda para un dominio con DNSSEC para verificar su correcto funcionamiento.

La herramienta DNSSEC Zone Inspector puede ser utilizada de dos maneras: desde la línea de comando o cómo una librería de Java. A continuación se detalla el uso de cada una de ellas:

### Línea de comando

Podrá hacer uso de la herramienta llamada DNSSEC Zone Inspector simplemente corriendo el siguiente comando:

{% highlight bash %} 

java -jar mx-dnssec-validator.jar [-e <number>] -f <filename> [-h] [-l <arg>] [-o <filename>] [-r <Report Type>] [-w <number>]


{% endhighlight %}


#### Opciones

{% highlight bash %} 
-e <Número>
    Imprime solamente el número de errores especificados.

-f <Archivo de zona>
    Ruta del archivo de zona a la que se validará la configuración DNSSEC.

-h
    Muestra el mensaje de ayuda.

-l <arg>
    Lenguaje del reporte de salida. Lenguajes soportados ES|EN

-o <Archivo de salida>
    Escribe el reporte de la zona en el archivo especificado. Si no es especificado el archivo de salida se usará la salida estándar de la consola.

-r <Tipo de reporte>
    Especifica el formato del reporte. Los tipos soportados son XML y TXT, siendo TXT el predefinido.

-w < Número >
    Imprime solamente el número de advertencias especificadas.
{% endhighlight %}

DNSSEC Zone Inspector regresa 1 como valor de salida si se encontraron errores al momento de realizar la validación de la zona. En caso de realizar la validación satisfactoriamente regresará el valor 0 y se podrá ver el reporte en el archivo especificado con la opción `-o`.

Si el reporte no contiene errores, la configuración DNSSEC y las firmas de la zona son consideradas como correctas y dicha zona estará lista para su uso en el servidor DNS autoritativo.


### Como Librería

Para usarlo como una dependencia para su aplicación Java, deberá hacerlo por medio de la clase ZoneValidator y el método verifyZoneFile, el cual regresa un objeto Report con los resultados de la validación.

{% highlight java %} 


package mx.nic.dnssec.test;

import java.io.IOException;
import java.security.Security;

import mx.nic.dnssec.Report;
import mx.nic.dnssec.ZoneValidator;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

public class ZoneValidatorTest {
    
	public static void main(String[] args) throws IOException {
		Security.addProvider(new BouncyCastleProvider());
		
		String zoneFile = "zonefileinspector.mx.zone.signed";
		
		ZoneValidator zoneValidator = new ZoneValidator(zoneFile);
		Report report = zoneValidator.verifyZoneFile();
		
		if(report.getTotalErrors() == 0) {
			System.out.println("passed");
		} else {
			System.out.println("failed with " + 
				report.getTotalErrors() + " error(s).");
		}
	}
	
}


{% endhighlight %}


### Estadísticas

Zone File Inspector es una herramienta de verificación que además brinda estadísticas de la zona verificada. Al realizar una validación, el sistema arroja estadísticas de la zona, el siguiente es un ejemplo:


{% highlight bash%} 


INFO:
    errores:   1392022 (mostrando: 100)
	Advertencias: 696010(mostrando: 100)
ESTADÍSTICAS:
	RR                   4,559,600
	DNSKEY                       2
	RRSET                2,269,138
	RRSIG                1,392,021
	RRSIGs válidos       1,392,020
	NSEC NSEC3             696,009
	NSEC NSEC3 válidos     696,009
	glue RR                  3,283
	OPT-OUT                177,625
	ENT                          1


{% endhighlight %}

### Bibliografía


* http://tools.ietf.org/html/rfc4033
* http://tools.ietf.org/html/rfc4034
* http://tools.ietf.org/html/rfc4035
* http://tools.ietf.org/html/rfc4470
* http://tools.ietf.org/html/rfc4641
* http://tools.ietf.org/html/rfc5155



	
